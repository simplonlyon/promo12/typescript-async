/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Person.ts":
/*!***********************!*\
  !*** ./src/Person.ts ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
var Person = /** @class */ (function () {
    function Person(name, surname, age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }
    return Person;
}());
exports.Person = Person;
/*
export class Person {
    name:string;
    surname:string;
    age:number;
    
    constructor(name:string, surname:string, age:number) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }
}


export class Person {
    constructor(name:string, surname:string, age:number) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }
}
*/ 


/***/ }),

/***/ "./src/ajax.ts":
/*!*********************!*\
  !*** ./src/ajax.ts ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * La classe XMLHttpRequest est la classe historique du JS permettant
 * de faire des requêtes HTTP (ou requête ajax). Cela va permettre au
 * code JS d'accéder à des data et de déclencher des modification
 * (via une API REST par exemple) directement sans rechargement de
 * page.
 */
var ajax = new XMLHttpRequest();
/**
 * Les requêtes ajax sont par défaut asynchrone. Cela signifie qu'elles
 * ne stopperont pas l'exécution du javascript le temps d'être résolue.
 * Cela signifie également qu'il va falloir mettre en place une stratégie
 * pour indiquer au JS quoi faire au moment où la réponse de la requête
 * sera disponible.
 * Pour ça, on utilise un système d'event qui se déclencheront à des
 * moment spécifique de la requête ajax. Ici l'event se déclenchera
 * à chaque fois que la propriété readystate de l'objet ajax sera modifiée
 * (c'est une propriété qui indique l'état actuel de la requête http)
 */
ajax.addEventListener('readystatechange', function () {
    //si le readyState vaut 4 (ou XMLHttpRequest.DONE), c'est que la requête
    //est terminée et la réponse est disponible
    if (ajax.readyState === ajax.DONE) {
        //On fait ce qu'on veut faire avec notre réponse
        console.log(ajax.response);
    }
});
//On prépare la requête ajax en indiquant la méthode HTTP et l'url de la ressource à requêter
ajax.open('GET', 'tsconfig.json');
//On déclenche la requête
ajax.send();


/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.__esModule = true;
var Person_1 = __webpack_require__(/*! ./Person */ "./src/Person.ts");
__webpack_require__(/*! ./ajax */ "./src/ajax.ts");
/** Dans du typescript on peut faire du javascript classique sans soucis */
console.log('bloup');
//Quand on déclare une variable, son type sera le type de la première
//valeur qu'on lui assignera, ici du string
var maVariable = 'bloup';
//Une fois une variable typé, par défaut typescript refuse qu'on mette
//quoique ce soit d'autre comme type à l'intérieur
maVariable = '10';
//On peut typer explicitement n'importe quel variable avec cette syntaxe
var unNombre;
unNombre = 10;
/**
 * On peut typer directement les arguments d'une fonction de la même manière
 * ainsi que le type de retour comme on le ferait en PHP
 */
function maFonction(prenom) {
    return 'salut ' + prenom;
}
maFonction('Jack');
var instance = new Person_1.Person('Jackson', 'Jack', 50);
var toutou = {
    name: 'fido',
    breed: 'corgi',
    birthdate: new Date()
};


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL1BlcnNvbi50cyIsIndlYnBhY2s6Ly8vLi9zcmMvYWpheC50cyIsIndlYnBhY2s6Ly8vLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDaENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUM5QmE7QUFDYjtBQUNBLGVBQWUsbUJBQU8sQ0FBQyxpQ0FBVTtBQUNqQyxtQkFBTyxDQUFDLDZCQUFRO0FBQ2hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC50c1wiKTtcbiIsIlwidXNlIHN0cmljdFwiO1xuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBQZXJzb24gPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gUGVyc29uKG5hbWUsIHN1cm5hbWUsIGFnZSkge1xuICAgICAgICB0aGlzLm5hbWUgPSBuYW1lO1xuICAgICAgICB0aGlzLnN1cm5hbWUgPSBzdXJuYW1lO1xuICAgICAgICB0aGlzLmFnZSA9IGFnZTtcbiAgICB9XG4gICAgcmV0dXJuIFBlcnNvbjtcbn0oKSk7XG5leHBvcnRzLlBlcnNvbiA9IFBlcnNvbjtcbi8qXG5leHBvcnQgY2xhc3MgUGVyc29uIHtcbiAgICBuYW1lOnN0cmluZztcbiAgICBzdXJuYW1lOnN0cmluZztcbiAgICBhZ2U6bnVtYmVyO1xuICAgIFxuICAgIGNvbnN0cnVjdG9yKG5hbWU6c3RyaW5nLCBzdXJuYW1lOnN0cmluZywgYWdlOm51bWJlcikge1xuICAgICAgICB0aGlzLm5hbWUgPSBuYW1lO1xuICAgICAgICB0aGlzLnN1cm5hbWUgPSBzdXJuYW1lO1xuICAgICAgICB0aGlzLmFnZSA9IGFnZTtcbiAgICB9XG59XG5cblxuZXhwb3J0IGNsYXNzIFBlcnNvbiB7XG4gICAgY29uc3RydWN0b3IobmFtZTpzdHJpbmcsIHN1cm5hbWU6c3RyaW5nLCBhZ2U6bnVtYmVyKSB7XG4gICAgICAgIHRoaXMubmFtZSA9IG5hbWU7XG4gICAgICAgIHRoaXMuc3VybmFtZSA9IHN1cm5hbWU7XG4gICAgICAgIHRoaXMuYWdlID0gYWdlO1xuICAgIH1cbn1cbiovIFxuIiwiLyoqXG4gKiBMYSBjbGFzc2UgWE1MSHR0cFJlcXVlc3QgZXN0IGxhIGNsYXNzZSBoaXN0b3JpcXVlIGR1IEpTIHBlcm1ldHRhbnRcbiAqIGRlIGZhaXJlIGRlcyByZXF1w6p0ZXMgSFRUUCAob3UgcmVxdcOqdGUgYWpheCkuIENlbGEgdmEgcGVybWV0dHJlIGF1XG4gKiBjb2RlIEpTIGQnYWNjw6lkZXIgw6AgZGVzIGRhdGEgZXQgZGUgZMOpY2xlbmNoZXIgZGVzIG1vZGlmaWNhdGlvblxuICogKHZpYSB1bmUgQVBJIFJFU1QgcGFyIGV4ZW1wbGUpIGRpcmVjdGVtZW50IHNhbnMgcmVjaGFyZ2VtZW50IGRlXG4gKiBwYWdlLlxuICovXG52YXIgYWpheCA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuLyoqXG4gKiBMZXMgcmVxdcOqdGVzIGFqYXggc29udCBwYXIgZMOpZmF1dCBhc3luY2hyb25lLiBDZWxhIHNpZ25pZmllIHF1J2VsbGVzXG4gKiBuZSBzdG9wcGVyb250IHBhcyBsJ2V4w6ljdXRpb24gZHUgamF2YXNjcmlwdCBsZSB0ZW1wcyBkJ8OqdHJlIHLDqXNvbHVlLlxuICogQ2VsYSBzaWduaWZpZSDDqWdhbGVtZW50IHF1J2lsIHZhIGZhbGxvaXIgbWV0dHJlIGVuIHBsYWNlIHVuZSBzdHJhdMOpZ2llXG4gKiBwb3VyIGluZGlxdWVyIGF1IEpTIHF1b2kgZmFpcmUgYXUgbW9tZW50IG/DuSBsYSByw6lwb25zZSBkZSBsYSByZXF1w6p0ZVxuICogc2VyYSBkaXNwb25pYmxlLlxuICogUG91ciDDp2EsIG9uIHV0aWxpc2UgdW4gc3lzdMOobWUgZCdldmVudCBxdWkgc2UgZMOpY2xlbmNoZXJvbnQgw6AgZGVzXG4gKiBtb21lbnQgc3DDqWNpZmlxdWUgZGUgbGEgcmVxdcOqdGUgYWpheC4gSWNpIGwnZXZlbnQgc2UgZMOpY2xlbmNoZXJhXG4gKiDDoCBjaGFxdWUgZm9pcyBxdWUgbGEgcHJvcHJpw6l0w6kgcmVhZHlzdGF0ZSBkZSBsJ29iamV0IGFqYXggc2VyYSBtb2RpZmnDqWVcbiAqIChjJ2VzdCB1bmUgcHJvcHJpw6l0w6kgcXVpIGluZGlxdWUgbCfDqXRhdCBhY3R1ZWwgZGUgbGEgcmVxdcOqdGUgaHR0cClcbiAqL1xuYWpheC5hZGRFdmVudExpc3RlbmVyKCdyZWFkeXN0YXRlY2hhbmdlJywgZnVuY3Rpb24gKCkge1xuICAgIC8vc2kgbGUgcmVhZHlTdGF0ZSB2YXV0IDQgKG91IFhNTEh0dHBSZXF1ZXN0LkRPTkUpLCBjJ2VzdCBxdWUgbGEgcmVxdcOqdGVcbiAgICAvL2VzdCB0ZXJtaW7DqWUgZXQgbGEgcsOpcG9uc2UgZXN0IGRpc3BvbmlibGVcbiAgICBpZiAoYWpheC5yZWFkeVN0YXRlID09PSBhamF4LkRPTkUpIHtcbiAgICAgICAgLy9PbiBmYWl0IGNlIHF1J29uIHZldXQgZmFpcmUgYXZlYyBub3RyZSByw6lwb25zZVxuICAgICAgICBjb25zb2xlLmxvZyhhamF4LnJlc3BvbnNlKTtcbiAgICB9XG59KTtcbi8vT24gcHLDqXBhcmUgbGEgcmVxdcOqdGUgYWpheCBlbiBpbmRpcXVhbnQgbGEgbcOpdGhvZGUgSFRUUCBldCBsJ3VybCBkZSBsYSByZXNzb3VyY2Ugw6AgcmVxdcOqdGVyXG5hamF4Lm9wZW4oJ0dFVCcsICd0c2NvbmZpZy5qc29uJyk7XG4vL09uIGTDqWNsZW5jaGUgbGEgcmVxdcOqdGVcbmFqYXguc2VuZCgpO1xuIiwiXCJ1c2Ugc3RyaWN0XCI7XG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xudmFyIFBlcnNvbl8xID0gcmVxdWlyZShcIi4vUGVyc29uXCIpO1xucmVxdWlyZShcIi4vYWpheFwiKTtcbi8qKiBEYW5zIGR1IHR5cGVzY3JpcHQgb24gcGV1dCBmYWlyZSBkdSBqYXZhc2NyaXB0IGNsYXNzaXF1ZSBzYW5zIHNvdWNpcyAqL1xuY29uc29sZS5sb2coJ2Jsb3VwJyk7XG4vL1F1YW5kIG9uIGTDqWNsYXJlIHVuZSB2YXJpYWJsZSwgc29uIHR5cGUgc2VyYSBsZSB0eXBlIGRlIGxhIHByZW1pw6hyZVxuLy92YWxldXIgcXUnb24gbHVpIGFzc2lnbmVyYSwgaWNpIGR1IHN0cmluZ1xudmFyIG1hVmFyaWFibGUgPSAnYmxvdXAnO1xuLy9VbmUgZm9pcyB1bmUgdmFyaWFibGUgdHlww6ksIHBhciBkw6lmYXV0IHR5cGVzY3JpcHQgcmVmdXNlIHF1J29uIG1ldHRlXG4vL3F1b2lxdWUgY2Ugc29pdCBkJ2F1dHJlIGNvbW1lIHR5cGUgw6AgbCdpbnTDqXJpZXVyXG5tYVZhcmlhYmxlID0gJzEwJztcbi8vT24gcGV1dCB0eXBlciBleHBsaWNpdGVtZW50IG4naW1wb3J0ZSBxdWVsIHZhcmlhYmxlIGF2ZWMgY2V0dGUgc3ludGF4ZVxudmFyIHVuTm9tYnJlO1xudW5Ob21icmUgPSAxMDtcbi8qKlxuICogT24gcGV1dCB0eXBlciBkaXJlY3RlbWVudCBsZXMgYXJndW1lbnRzIGQndW5lIGZvbmN0aW9uIGRlIGxhIG3Dqm1lIG1hbmnDqHJlXG4gKiBhaW5zaSBxdWUgbGUgdHlwZSBkZSByZXRvdXIgY29tbWUgb24gbGUgZmVyYWl0IGVuIFBIUFxuICovXG5mdW5jdGlvbiBtYUZvbmN0aW9uKHByZW5vbSkge1xuICAgIHJldHVybiAnc2FsdXQgJyArIHByZW5vbTtcbn1cbm1hRm9uY3Rpb24oJ0phY2snKTtcbnZhciBpbnN0YW5jZSA9IG5ldyBQZXJzb25fMS5QZXJzb24oJ0phY2tzb24nLCAnSmFjaycsIDUwKTtcbnZhciB0b3V0b3UgPSB7XG4gICAgbmFtZTogJ2ZpZG8nLFxuICAgIGJyZWVkOiAnY29yZ2knLFxuICAgIGJpcnRoZGF0ZTogbmV3IERhdGUoKVxufTtcbiJdLCJzb3VyY2VSb290IjoiIn0=