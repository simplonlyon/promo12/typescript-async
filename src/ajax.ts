
/**
 * La classe XMLHttpRequest est la classe historique du JS permettant
 * de faire des requêtes HTTP (ou requête ajax). Cela va permettre au
 * code JS d'accéder à des data et de déclencher des modification 
 * (via une API REST par exemple) directement sans rechargement de
 * page.
 */
let ajax = new XMLHttpRequest();
/**
 * Les requêtes ajax sont par défaut asynchrone. Cela signifie qu'elles
 * ne stopperont pas l'exécution du javascript le temps d'être résolue.
 * Cela signifie également qu'il va falloir mettre en place une stratégie
 * pour indiquer au JS quoi faire au moment où la réponse de la requête
 * sera disponible.
 * Pour ça, on utilise un système d'event qui se déclencheront à des
 * moment spécifique de la requête ajax. Ici l'event se déclenchera
 * à chaque fois que la propriété readystate de l'objet ajax sera modifiée
 * (c'est une propriété qui indique l'état actuel de la requête http)
 */
ajax.addEventListener('readystatechange', function () {
    //si le readyState vaut 4 (ou XMLHttpRequest.DONE), c'est que la requête
    //est terminée et la réponse est disponible
    if (ajax.readyState === ajax.DONE) {
        //On fait ce qu'on veut faire avec notre réponse
        console.log(ajax.response);
    }
});
//On prépare la requête ajax en indiquant la méthode HTTP et l'url de la ressource à requêter
ajax.open('GET', 'tsconfig.json');
//On déclenche la requête
ajax.send();