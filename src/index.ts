import { Person } from "./Person";
import { Dog } from "./Dog";

import './ajax';

/** Dans du typescript on peut faire du javascript classique sans soucis */
console.log('bloup');
//Quand on déclare une variable, son type sera le type de la première
//valeur qu'on lui assignera, ici du string
let maVariable = 'bloup';
//Une fois une variable typé, par défaut typescript refuse qu'on mette
//quoique ce soit d'autre comme type à l'intérieur
maVariable = '10';
//On peut typer explicitement n'importe quel variable avec cette syntaxe
let unNombre:number;

unNombre = 10;

/**
 * On peut typer directement les arguments d'une fonction de la même manière
 * ainsi que le type de retour comme on le ferait en PHP
 */
function maFonction(prenom:string):string {
    return 'salut '+prenom;
}

maFonction('Jack');

let instance:Person = new Person('Jackson', 'Jack', 50);


let toutou:Dog = {
    name: 'fido', 
    breed:'corgi', 
    birthdate: new Date()
};