export class Person {
    
    constructor(public name:string, 
                public surname:string, 
                public age:number) {}

}

/*
export class Person {
    name:string;
    surname:string;
    age:number;
    
    constructor(name:string, surname:string, age:number) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }
}


export class Person {    
    constructor(name:string, surname:string, age:number) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }
}
*/