export interface Dog {
    name:string;
    breed:string;
    birthdate:Date;
}